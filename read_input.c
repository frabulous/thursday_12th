#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "print_output.h"

int readInt() //TODO
{
    char* end;
    char buffer[128];
    int count = 0;
    int input;

    do {
        if(count>0){
            printf("Input non valido, inserire un numero:");
            count = 0;
        }
        printf("\n>  ");
        if(!fgets(buffer, sizeof(buffer), stdin))
            break;

        buffer[strlen(buffer)-1] = 0; //replace '\n' with '\0'

        input = strtol(buffer, &end, 10);
        
        count = 1;
    }
    while (end != buffer + strlen(buffer));
    count = 0;
    
    return input;
}

const char* readKeyword(){
    char buffer[128];
    char* input;

    input = malloc(48);
    
    printf("\n>  ");
    fgets(buffer, sizeof(buffer), stdin);

    int c;
    int i;
    for(i=0; i<47; i++){
        c = buffer[i];
        if((c>='a' && c<='z') || c=='_')
        {
            input[i] = (char)c;
        }
        else
            break;
    }
    input[i] = '\0';

    return input;
}