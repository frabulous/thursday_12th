#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>

#define ANSI_COLOR_RESET    "\x1b[0m"
#define ANSI_COLOR_RED      "\x1b[31m"
#define ANSI_COLOR_GREEN    "\x1b[32m"
#define ANSI_COLOR_YELLOW   "\x1b[33m"
#define ANSI_COLOR_BLUE     "\x1b[34m"
#define ANSI_COLOR_MAGENTA  "\x1b[35m"
#define ANSI_COLOR_CYAN     "\x1b[36m"

void clean(){
    system("clear");
}

void printMessage(const char* string){
    printf("%s", string);
    while((char)getchar() != '\n'){/*busy*/}
    clean();
}
void printReport(const char* string){
    printf("%s\n", string);
    getchar();
}

void printTitle(const char* string){
    clean();
    char c = string[0];
    int i = 0;
    while(c != '\0'){
        printf("-");
        c = string[i++];
    }
    printf("-------------------------\n");
    printf("------------ %s ------------\n\n", string);
}

void printMenu(int count, ...){
    va_list list;
    va_start(list, count);
    for(int i=0; i<count; i++)
    {
        const char* command = va_arg(list, const char*);
        printf("%d - %s\n", i, command);
    }
    va_end(list);
}

void printGieson(const char* string){
    char c = string[0];
    int i = 0;
    clean();
    printf("\n\n\n           " ANSI_COLOR_RED);
    while(c!= '\0')
    {
        printf("%c", c);
        if(c == '.' && string[i+1] != '.')
        {
            usleep(100000);
        }
        else if(c =='\n') 
        {
            printf("           ");
        }
        i++;
        c = string[i];
        usleep(40000);
        fflush(stdout);
    }
    printf(ANSI_COLOR_RESET "\n\n\n");
    printMessage("> " ANSI_COLOR_MAGENTA "Avanti...\n\n" ANSI_COLOR_RESET);
}

void printRed(const char* string){
    printf(ANSI_COLOR_RED "%s" ANSI_COLOR_RESET, string);
}

void printHealth(const int state){
    printf("SALUTE:\t");
    if(state == 2)
        printf(ANSI_COLOR_GREEN "\u2764 \u2764 ");
    else if (state == 1)
        printf(ANSI_COLOR_YELLOW "\u2764 ");

    printf(ANSI_COLOR_RESET "\n\n");
}
