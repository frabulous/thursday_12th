#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "gamelib.h"
#include "read_input.h"
#include "print_output.h"

/* Questo file contiene solo la stampa del menu principale e poi richiama una delle
 tre funzioni possibili: create_map(), start(), quit(). */


static void printLaunchMenu();


int main (void) {

    extern time_t sys_time;
    srand((unsigned) time(&sys_time)); //reset random seed

    int exit_main = 0;
    do{
        printLaunchMenu();
        //get and check user input
        int in;
        int res;
        in = readInt();
        switch(in)
        {
            case 0:
                res = quit();
                if(res == 0)
                    exit_main = 1;
                break;
            case 1:
                res = create_map();
                break;
            case 2:
                res = start();
                if(res < 0){
                    printMessage("Per iniziare una nuova partita è necessario creare una mappa");
                }
                break;
            default:
                printMessage("Scegliere una fra le opzioni (0, 1 o 2) e premere Invio");
                break;
        }
    } while (!exit_main);

   return 0;
}

static void printLaunchMenu(){
    printTitle("GIOVEDI 12");
    printMenu(3, "Esci", "Nuova mappa", "Inizia partita");
}