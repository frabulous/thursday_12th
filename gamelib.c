#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "gamelib.h"
#include "read_input.h"
#include "print_output.h"

/*
From exam's file:
    Questo file contiene le definizioni di tutte le funzioni da implementare.
   Quelle visibili in main crea_mappa(), gioca(), termina_gioco() hanno linkage esterno.
   Tutte le funzioni non visibili in main.c e definite qui, devono avere linkage interno.
   Tutte le variabili globali definite in questo file devono avere linkage interno.
*/

#define SCORE_ESCAPE 150
#define SCORE_STEP 100
#define SCORE_HEALTH 500
#define SCORE_BOTH 1000 

static const int SCORE_BACKPACK[USABLE_ITEMS_N] = {
    800, 100, 200, 300, 600, 500
};

time_t sys_time;

static Area* first_area; //prima_zona
static Area* last_area; //ultima_zona 
static Player marzia;
static Player giacomo;
static int player_turn;

static int is_map_ready = 0;
static int map_size = 0;

static const int KEYWORD_MAX_LENGTH = 50;
static const int MIN_ZONES_N = 8;
static const int POSSIBLE_ZONES_N = 6;

typedef enum {
    both_death=-2, death, injury, avoid, dispel
} FightResult;

static const char* keywords[] = {
    "cucina", "soggiorno", "rimessa", "strada", "lungo_lago", "uscita_campeggio"
};
static const char* items[] = {
    "cianfrusaglia", "bende", "coltello", "pistola", "benzina", "adrenalina"
};
/*static const char* states[] = {
    "morto", "ferito", "vivo"
};*/

static const int table_rows[6][10] = {
    {0, 0, 0, 1, 1, 2, 2, 2, 2, 5},
    {0, 0, 1, 2, 3, 3, 3, 5, 5, 5},
    {0, 0, 1, 2, 2, 2, 4, 4, 4, 5},
    {0, 0, 0, 0, 0, 0, 0, 0, 2, 4},
    {0, 0, 0, 0, 0, 0, 0, 2, 4, 4},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 2}
};

Player* p_playing[2] = {NULL, NULL};

//--------------------------
//-- DECLARATION of funcs --

static void print_map(); //stampa_mappa()
static int close_map(); //chiudi_mappa()
static void canc_area(); //canc_zona()
//running-game
static Player* initPlayer();
static void printPath(Player* p);
static void printPlayer(Player* p);
static int handlePlayerFight(int count, Player* p_list[]);
static int playTurn();
static int handlePlayerMenu(Player* player);
static void die(Player* player);
static void escape(Player* player);
static int go_forward(Player* player);
static int search_item(Player* player);
static int pick_item(Player* player);
static int heal(Player* player);
static int use_adrenalin(Player* player);
static int use_knife(Player* player);
static int use_handgun(Player* player);
static int use_gasoline(Player* player);
static int getPlayerScore(Player* p);
static int getScore();
static void printIntro();
//map-creation
static void ins_area(Area* p_area); //ins_zona()
static void assignItem(Area* p_area);
static int generateAreaByKeyword(char keyword[KEYWORD_MAX_LENGTH]);
static int loadMapFromFile();
static int loadMapManually();
static void clearMap();
static void mapRandomize(int size);

//-------------------------
//-- DEFINITION of funcs --

int create_map()
{
    int end = 0;
    while(!end){
        printTitle("MAPPA");
        printMenu(4, "Indietro", "Carica mappa da file", "Crea mappa dall'editor", "Genera una mappa casuale");

        int in;
        in = readInt();

        switch(in)
        {
            case 0:
                if(is_map_ready) end = 1;
                else return -1;
                break;
            case 1:
                if(loadMapFromFile() < 0)
                    return -2;
                end = 1;
                break;
            case 2:
                if(loadMapManually() < 0) 
                    return -3;
                end = 1;
                break;
            case 3:
                printf("%s (%s %d):\n", "Inserire il numero di zone", "almeno", MIN_ZONES_N);
                mapRandomize(readInt());
                end = 1;
                break;
            default:
                printMessage("Comando non valido, digitare uno dei possibili valori (0, 1, 2 o 3)");
                continue;
        }
    }
    return 0;
}

int start()
{
    if(!is_map_ready) return -1;

    printIntro();

    int return_value = 0;

    //init game objects
    marzia = *initPlayer();
    marzia.backpack[adrenalin] = 2;
    giacomo = *initPlayer();
    giacomo.backpack[knife] = 1;

    p_playing[0] = &marzia;
    p_playing[1] = &giacomo;
    
    int turn_count = 1;
    int gasoline_count = 0;

    clean();
    printMessage("> INIZIA");

    //game loop:
    int playing = 1;
    while(playing){

        //exit controls
        if (marzia.state == dead && giacomo.state == dead){
            printMessage("Gieson continuerà ancora ad infestare il campeggio indisturbato.");
            return_value = 0;
            break;
        }
        else if (marzia.escaped && giacomo.state == dead){
            printMessage("Marzia è riuscita a scappare, ma Gieson ha preso Giacomo...");
            return_value = 2;
            break;
        }
        else if (marzia.state == dead && giacomo.escaped){
            printMessage("Giacomo è riuscito a scappare, ma Gieson ha preso Marzia...");
            return_value = 3;
            break;
        }

        //printf("\nInizio turno %d", turn_count);
        //printMessage("");

        if(marzia.state > dead && giacomo.state > dead && !marzia.escaped && !giacomo.escaped){
            //player_turn: 
            // 0 means Marzia, 
            // 1 means Giacomo
            player_turn = rand() % 2;
            for(int i=1; i>0; i--){
                int res = playTurn();
                if(res == 4)
                    i +=2 ;
            }
            if(player_turn) 
                printMessage("> Passa il controllo a Marzia");
            else
                printMessage("> Passa il controllo a Giacomo");

            player_turn++;
            for(int i=1; i>0; i--){
                int res = playTurn();
                if(res == 4)
                    i +=2 ;
            }
        }
        else{
            if(marzia.state>dead && (giacomo.state <= dead || giacomo.escaped))
                player_turn = 0; //Only Marzia is playing
            else if(giacomo.state>dead && (marzia.state <= dead || marzia.escaped))
                player_turn = 1; //Only Giacomo is playing
            else
                break; //both are dead

            for(int i=1; i>0; i--){
                if(playTurn() == 4)
                    i +=2 ;
            }
        }

        //exit control
        if (p_playing[0] == NULL && p_playing[1] == NULL){
            printMessage("Marzia e Giacomo sono sopravvissuti!");
            return_value = 1;
            break;
        }

        printf("\nFine del turno %d", turn_count);
        printMessage("");
        turn_count += 1;

        if(gasoline_count > 0){
            gasoline_count -=1;
            printMessage("Gieson non tornerà, per il momento");
            continue; //skip Gieson
        }

        ////randomize Gieson
        int gieson_prob = 30;
        if(p_playing[0] == NULL || p_playing[1] == NULL)
            gieson_prob = 50;
        if(marzia.position == last_area || giacomo.position == last_area)
            gieson_prob = 75;

        int g_spawn = rand()%100;
        //printf("Gieson spawn: %d", g_spawn);
        //printReport("");
        if(g_spawn < gieson_prob)
        {
            //Gieson appears
            int speech = rand()%4;
            if(speech==0)
                printGieson("Si sente un rumore provenire dal buio...\n\nGieson è qui!");
            else if(speech==1)
                printGieson("La sagoma di una bianca maschera fende l'oscurità...\n\nGieson è qui!");
            else if(speech==2)
                printGieson("Dei passi in lontananza...\nSi fanno sempre più vicini...\n\nGieson è qui!");
            else
                printGieson("C'è silenzio...\nUn respiro affannato echeggia nell'aria...\n\nGieson è qui!");

            if(marzia.position == giacomo.position && p_playing[0] != NULL && p_playing[1] != NULL)
            {
                printReport("> Gieson ha raggiunto Marzia e Giacomo");
                Player* array[2] = {&marzia, &giacomo};
                int res;
                res = handlePlayerFight(2, array);

                if (res == both_death)
                    printMessage("> Gieson ha avuto la meglio su Marzia e Giacomo");
                else if (res == death){
                    if(giacomo.state != dead) printMessage("> Gieson ha avuto la meglio su Marzia");
                    if(marzia.state != dead) printMessage("> Gieson ha avuto la meglio su Giacomo");
                }
                else if (res == dispel)
                    gasoline_count = 4;
            }
            else //Gieson separately
            {
                // Gieson attacks the furthest one, first
                int i;
                int count = 0;

                if(marzia.steps > giacomo.steps) 
                    i=0;
                else
                    i=1;
                
                while(count < 2){
                    if(p_playing[i] != NULL){

                        if(!i)
                            printReport("> Gieson ha raggiunto Marzia");
                        else
                            printReport("> Gieson ha raggiunto Giacomo");

                        Player* array[1] = {p_playing[i]};

                        int res;
                        res = handlePlayerFight(1, array);
                        
                        if (res == death){
                            if(!i)
                                printMessage("> Gieson ha avuto la meglio su Marzia");
                            else
                                printMessage("> Gieson ha avuto la meglio su Giacomo");
                        }
                        else if (res == dispel){
                            gasoline_count = 4;
                            count = 9; //if first player uses gasoline, Gieson doesn't attack the second one
                        }
                    }
                    count += 1;
                    i = (i+1)%2;
                }

            }
        }

    }//end of game loop

    getScore();
    printMessage("\n> Termina\n");

    clearMap();

    return return_value;
}

int quit()
{
    while(1){
        printTitle("Chiudere il gioco?");
        printMenu(2, "Indietro", "Chiudi");

        int in;
        in = readInt();

        switch(in)
        {
            case 0:
                return -1;
                break;
            case 1:
                clearMap();
                printf("\n>>>>>>>>>>>> %s <<<<<<<<<<<<\n\n", "GAME OVER");
                return 0;
                break;
            default: 
                printMessage("Comando non valido, digitare uno dei possibili valori (0 o 1)");
                continue;
        }
    }
}

static Player* initPlayer()
{
    Player* p;
    p = malloc(sizeof(Player));
    p->state = alive;
    p->position = first_area;
    p->steps = 0;
    p->escaped = 0;
    for(int i=0; i<USABLE_ITEMS_N; i++){
        p->backpack[i] = 0;
    }
    return p;
}

static void printPath(Player* p)
{
    Area* current = first_area;
    printf("POSIZIONE:\n\t|-");
    for(int i=0; i<map_size; i++)
    {
        if(p->steps == i){
            printf("--%s-", keywords[p->position->area]);
            if(p->position == last_area)
                printf("--|");
            else
                printf("-->");
            
            break;
        }
        else printf("--o-");

        current = current->next_area;
    }
    printf("\n\n");
}

static void printPlayer(Player* p)
{
    printHealth(p->state);
    printPath(p);
    printf("%s:\n", "ZAINO");
    int count = 0;
    for(int i=0; i<USABLE_ITEMS_N; i++){
        if(p->backpack[i] > 0){
            printf("\t- %s (%d)\n", items[i], p->backpack[i]);
            count++;
        }
    }
    if(count == 0) printf("\tvuoto");
    printf("\n\n");
}

static int handlePlayerFight(int count, Player* p_list[])
{
    int (*f[count*3])(Player* player);
    Player* p_array[count*3];
    int i = 0;
    Player* current_player;

    for(int k=0; k<count; k++)
    {
        current_player = p_list[k];
        
        if(current_player == &marzia) printf("Marzia:\n");
        else if(current_player == &giacomo) printf("Giacomo:\n");

        if(current_player->backpack[knife] > 0){
            f[i] = use_knife;
            p_array[i] = current_player;
            i++;
            printf("\t%d - %s \n", i, "usa coltello");
        }
        if(current_player->backpack[handgun] > 0){
            f[i] = use_handgun;
            p_array[i] = current_player;
            i++;
            printf("\t%d - %s \n", i, "usa pistola");
        }
        if(current_player->backpack[gasoline] > 0){
            f[i] = use_gasoline;
            p_array[i] = current_player;
            i++;
            printf("\t%d - %s \n", i, "usa benzina");
        }
        if(i == 0 || p_array[i-1] != current_player)
            printf("\tnon possiede armi.\n");
    }

    if(i == 0) //No option available => player(s) death
    {
        for(int k=0; k<count; k++){
            die(p_list[k]);
        }

        printMessage("> Non c'è modo di difendersi...");
        if(count == 1) return death;
        return both_death;
    }
    else
    {
        int in;
        do{
            in = readInt();
        }
        while(in <= 0 || in > i);

        int res;
        res = (*f[in-1])(p_array[in-1]);
        
        return res;
    }
}

static int playTurn()
{
    Player* player;
    if(player_turn % 2){
        player = &giacomo;
        printTitle("< Giacomo >");
    }
    else {
        player = &marzia;
        printTitle("< Marzia >");
    }
    printPlayer(player);
    int res;
    res = handlePlayerMenu(player);

    return res;
}

static int handlePlayerMenu(Player* player)
{
    int (*f[4])(Player* player);
    int i = 0;

    printf("%s:\n", "AZIONI");

    f[i] = go_forward;
    i++;
    if(player->position != last_area)
        printf("%d - %s\n", i, "Avanza");
    else
        printf("%d - %s\n", i, "Scappa!");
    
    if(!player->position->is_searched){
        f[i] = search_item;
        i++;
        printf("%d - %s\n", i, "Rovista");
    }
    else {
        f[i] = pick_item;
        i++;
        printf("%d - %s %s\n", i, "Raccogli", items[player->position->item]);
    }

    if(player->backpack[bandages] > 0){
        f[i] = heal;
        i++;
        if(marzia.position == giacomo.position)
            printf("%d - %s\n", i, "Cura entrambi");
        else
            printf("%d - %s\n", i, "Curati");
    }

    if(player->backpack[adrenalin] > 0){
        f[i] = use_adrenalin;
        i++;
        printf("%d - %s\n", i, "Usa adrenalina");
    }

    int in;
    do{
        in = readInt();
    }
    while(in <= 0 || in > i);

    return (*f[in-1])(player);
    
}

static void die(Player* player){
    player->state = dead;
    player->position = NULL;
    if(player == &marzia) p_playing[0] = NULL;
    else if (player == &giacomo) p_playing[1] = NULL;
}
static void escape(Player* player)
{
    if(player == &marzia){
        p_playing[0] = NULL;
        printf("Marzia è riuscita a scappare dal campeggio!\n");
    }
    else if(player == &giacomo){
        p_playing[1] = NULL;
        printf("Giacomo è riuscito a scappare dal campeggio!\n");
    }
    player->escaped = 1;
    player->position = NULL;
}

static int go_forward(Player* player)
{
    player->steps += 1;
    if(player->position != last_area){
        player->position = player->position->next_area;
        if(player == &marzia)
            printf("Marzia avanza in ");
        else if (player == &giacomo)
            printf("Giacomo avanza in ");
        printf("%s", keywords[player->position->area]);
        printReport("\n");
        return 0;
    }
    else{
        escape(player);
        return 5;
    }
}
static int search_item(Player* player)
{
    printf("Hai trovato ");
    printReport(items[player->position->item]);
    player->position->is_searched = 1;
    return 1;
}
static int pick_item(Player* player)
{
    printf("Hai raccolto: %s\n", items[player->position->item]);
    player->backpack[player->position->item] += 1;
    player->position->item = rubbish;
    return 2;
}
static int heal(Player* player)
{
    player->backpack[bandages] -= 1;
    if(marzia.position == giacomo.position && marzia.state > dead && giacomo.state > dead){
        marzia.state = alive;
        giacomo.state = alive;
        printReport("Marzia e Giacomo ora sono in salute.");
    }
    else{
        player->state = alive;
        printReport("La salute sale al massimo.");
    }
    return 3;
}
static int use_adrenalin(Player* player)
{
    printReport("L'adrenalina entra in circolo,\ni tuoi movimenti sono accelerati!\n");
    player->backpack[adrenalin] -= 1;
    return 4;
}
static int use_knife(Player* player)
{
    player->backpack[knife] -= 1;
    if(player->state == alive){
        printf("Gieson si allontana,\n");
        if(player == &giacomo)
            printReport("ma Giacomo rimane ferito nello scontro.");
        else if(player == &marzia)
            printReport("ma Marzia rimane ferita nello scontro.");
        player->state = injured;
        return injury;
    }
    else if(player->state == injured){
        printReport("Gieson è scappato,\nma non sei sopravvissuto allo scontro.");
        die(player);
    }
    return death;
}
static int use_handgun(Player* player)
{
    printReport("Colpisci Gieson,\nche si rifugia nell'oscurità.");
    player->backpack[handgun] -= 1;
    return avoid;
}
static int use_gasoline(Player* player)
{
    printReport("Gieson, in fiamme, scappa verso il lago.\nNon tornerà, per il momento...");
    player->backpack[gasoline] -= 1;
    return dispel;
}

static int getPlayerScore(Player* p){
    int p_score = 0;
    int tmp = 0;

    tmp = p->steps*SCORE_STEP;
    p_score += tmp;
    printf("BONUS AVANZAMENTO:\t+%d\n", tmp);

    if(p->state == alive){
        p_score += SCORE_HEALTH;
    }
    printf("BONUS SALUTE:\t\t+%d\n", p_score-tmp);
    tmp = p_score;

    for(int i=0; i<USABLE_ITEMS_N; i++){
        const int q = p->backpack[i];
        p_score += q*SCORE_BACKPACK[i];
    }
    printf("BONUS OGGETTI:\t\t+%d\n", p_score-tmp);
    tmp = p_score;

    if(p->escaped){
        p_score += map_size*SCORE_ESCAPE;
    }
    printf("BONUS FUGA:\t\t+%d\n", p_score-tmp);
    printf("\n\t\t\t= %d\n", p_score);

    return p_score;
}

static int getScore()
{
    int total = 0;

    printTitle("PUNTEGGI");
    printf("MARZIA\n");
    printf("******\n");
    total+= getPlayerScore(&marzia);
    printf("GIACOMO\n");
    printf("*******\n");

    total+= getPlayerScore(&giacomo);

    if(giacomo.escaped && marzia.escaped){
        total+= SCORE_BOTH;
        printf("\nBONUS SQUADRA:\t\t%d\n", SCORE_BOTH);
    }

    printf("\nPUNTEGGIO TOTALE = %d\n", total);

    return total;
}

static void printIntro()
{
    clean();
    printf("\n\n\n");
    printRed("         ___        ___           ___   ___    \\       .  _    \n");
    printRed("        //  _  ||  //  \\  \\\\  /  ||_   ||  \\  ||      /| ( )  \n");
    printRed("        \\\\__/  ||  \\\\__/   \\\\/   ||__  ||__/  ||      _|_ /__ \n");

    sleep(2);
    printMessage("");
    clean();

    printf("\n\n\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printf("~ È giovedì 12 giugno 1980 ed un gruppo di studenti universitari\n");
    printf("~ del Corso di Laurea in Informatica si trasferiscono\n");
    printf("~ in vacanza al “Campeggio Lake Trasymeno”, che sta per riaprire.\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printMessage("\n> Avanti");
    printf("\n\n\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printf("~ Ventitre anni prima, infatti, nello stesso campeggio, un ragazzino\n");
    printf("~ di nome Gieson era annegato per colpa della negligenza di un programmatore:\n");
    printf("~ a causa di un segmentation fault nel suo programma di noleggio delle barche,\n");
    printf("~ alla famiglia di Gieson era stata affidata una barca con un motore difettoso.\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printMessage("\n> Avanti");
    printf("\n\n\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printf("~ Gieson però infesta ancora il campeggio, e nutre vendetta\n");
    printf("~ nei confronti degli informatici poco attenti che hanno seguito\n");
    printf("~ il corso di Programmazione I giocando a League of Legends...\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printMessage("\n> Avanti");
    printf("\n\n\n");
    printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n");
    printf("~ Giacomo e Marzia, entrambi studenti del primo anno,\n");
    printf("~ rimangono isolati dagli altri quando...\n");
    printMessage("\n> ...");
}

//---------------
//--MAP FUNCTIONS

static void print_map() 
{
    Area* temp;
    temp = first_area;

    if(temp == NULL)
        printMessage("La mappa è vuota");
    else
    {
        printf(" -> ");
        while(temp != NULL)
        {
            switch(temp->area)
            {
                case kitchen:
                    printf("%s", "cucina");
                    break;
                case living_room:
                    printf("%s", "soggiorno");
                    break;
                case shed:
                    printf("%s", "rimessa");
                    break;
                case street:
                    printf("%s", "strada");
                    break;
                case lakefront:
                    printf("%s", "lungo_lago");
                    break;
                case camping_exit:
                    printf("%s", "uscita_campeggio");
                    break;
            }
            
            if(temp != last_area) printf("\n -> ");
            temp = temp->next_area;
        }
        printf("\n");
        printMessage("");
    }
}

static int close_map() 
{
    if(map_size < MIN_ZONES_N-1){
        printf("%s %d\n", "Impossibile terminare la mappa! Il numero minimo di zone è", MIN_ZONES_N-1);
        return -1;
    }
    Area* new_area = malloc(sizeof(Area));
    
    new_area->area = camping_exit;
    new_area->next_area = NULL;
    new_area->is_searched = 0;
    assignItem(new_area);
    ins_area(new_area);

    is_map_ready = 1;
    print_map();
    printMessage("Mappa creata correttamente!");
    return 1;
}

static void canc_area()
{
    if (map_size <= 0) {
        printMessage("Nessuna zona da eliminare");
        return;
    }
    Area* temp = first_area;
    
    
    if(temp == last_area) //case map_size==1 <-> first_area==last_area
    {
        free(temp);
        first_area = NULL;
        last_area = NULL;
        map_size = 0;
    }
    else
    {
        while(temp->next_area != last_area) //get the penultimate area
        {
            temp = temp->next_area;
        }
        free(temp->next_area);
        temp->next_area = NULL;
        last_area = temp;
        map_size -= 1;
    }
}

static void ins_area(Area* p_area)
{
    if(map_size == 0){
        first_area = p_area;
        last_area = p_area;
    }
    else{
        last_area->next_area = p_area;
        last_area = p_area;
    }

    map_size+=1;
}

static void assignItem(Area* p_area)
{
    //randomize the item to assign to the new area
    int r = rand()%10;
    int new_item = table_rows[p_area->area][r];
    p_area->item = new_item;
}

static int generateAreaByKeyword(char keyword[KEYWORD_MAX_LENGTH])
{
    Area* new_area = malloc(sizeof(Area));
    Type_area t_area;

    int correct = 0;

    for(int i=0; i<POSSIBLE_ZONES_N-1; i++){
        if(strcmp(keyword, keywords[i]) == 0) //input string is a keyword
        {
            t_area = i; //enum Type_zone
            correct = 1;
            break;
        }
    }
    if(!correct){
        free(new_area);
        return -1; //cannot create this area, reporting error
    }

    new_area->area = t_area;
    new_area->next_area = NULL;
    new_area->is_searched = 0;
    assignItem(new_area);
    ins_area(new_area);

    return 1; //area correctly created
}

static int loadMapFromFile()
{
    clearMap();

    char line[KEYWORD_MAX_LENGTH];
    char map_name[30];
    FILE* map_file;
    printf("%s: ", "Inserire il nome del file contenente la mappa");
    scanf("%s", map_name);
    

    map_file = fopen(map_name, "r");
    if(map_file == NULL){
        printMessage("Errore! Impossibile accedere al file");
        printMessage("");
        return -1;
    }
    int count = 0;
    while (!feof(map_file)) //read the file line-by-line
    {   
        fgets(line, KEYWORD_MAX_LENGTH, map_file);
        
        if(line[0]=='#' || line[0]=='\n') //ignore comments and blank lines
            continue;
        
        line[strcspn(line, "\n")] = '\0'; //remove the '\n' character from end of keyword
        if (generateAreaByKeyword(line) == 1){
            count+=1;
        }
        else{
            printMessage("Errore! Sintassi errata nel testo del file");
            fclose(map_file);
            return -2;
        }
    }
    fclose(map_file);

    if(close_map()){
        return 1;
    }
    else{
        clearMap();
        return -3;
    }
}

static int loadMapManually() 
{
    clearMap();

    int end = 0;
    while(!end)
    {
        //print menu
        printTitle("CREAZIONE DELLA MAPPA");
        printMenu(6, "Annulla creazione", "Inserire una nuova zona", "Cancellare l'ultima zona",
         "Visualizza zone create", "Concludi creazione", "Istruzioni");

        int res;
        char in_string[KEYWORD_MAX_LENGTH];
        //scan input
        int in;
        in = readInt();
        
        switch(in)
        {
            case 0:
                clearMap();
                return -1;
                break;
            case 1:
                printf("%s:\n", "Scegliere il tipo di zona da inserire");
                printf("%s - %s - %s - %s - %s\n", "cucina", "soggiorno", "rimessa", "strada", "lungo_lago");

                strcpy(in_string, readKeyword());
                               
                res = generateAreaByKeyword(in_string);

                if(res == 1)
                    printMessage("Zona aggiunta correttamente");
                else
                    printMessage("Parola inserita non corretta");
                
                break;
            case 2:
                canc_area();
                print_map();
                break;
            case 3:
                print_map();
                break;
            case 4:
                res = close_map();
                if (res==1){
                    end = 1;
                }
                break;
            case 5: 
                printTitle("COME USARE L'EDITOR");
                printf("> %s.\n", "Utilizzare il comando 'Inserire una nuova zona' per aggiungere zone alla mappa");
                printf("> %s:\n", "Le possibili zone sono le seguenti");
                printf("> %s - %s - %s - %s - %s\n", "cucina", "soggiorno", "rimessa", "strada", "lungo_lago");
                printf("> %s.\n", "Utilizzare il comando 'Concludi creazione' per finalizzare la mappa");
                printf("> %s: %d\n", "Il numero minimo di zone necessarie a concludere una mappa è", MIN_ZONES_N-1);
                printf("\n");
                printMessage("Torna all'editor");
                break;
            default:
                printMessage("Comando non valido, digitare uno dei possibili valori (0, 1, 2, 3, 4 o 5)");
                continue;
        }
    } 
    //map is ready
    return 1;
}

static void clearMap()
{
    int size = map_size;
    for(int i=0; i < size; i++)
    {
        canc_area();
    }
    is_map_ready = 0;
}

static void mapRandomize(int size)
{
    if (size < MIN_ZONES_N) size = MIN_ZONES_N;

    for(int i=0; i<size-1; i++)
    {
        int k = rand()%(POSSIBLE_ZONES_N-1);
        char k_string[KEYWORD_MAX_LENGTH];
        strcpy(k_string, keywords[k]);
        generateAreaByKeyword(k_string);
    }

    close_map();
}