/*
Header file of read_input library.
Use it to get user's input from keyboard.
*/
int readInt();

const char* readKeyword();