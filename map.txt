# SAMPLE MAP FILE
# Comments must start with '#' symbol,
# lines must not be longer than 48-------------|
# --_----_----_----_----_----_----_----_----_48|
#
# A map must contain 7 zones at least.
#
# For each zone you want to insert,
# write a line with relative keyword.
#
# Possible keywords:
# cucina,soggiorno,rimessa,strada,lungo_lago
#
cucina
strada
soggiorno
cucina
strada
lungo_lago
rimessa
strada
# uscita_campeggio will be automatically
# added as last area by the game.
