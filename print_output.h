/**/

void clean();
void printMessage(const char* string);
void printReport(const char* string);
void printTitle(const char* string);
void printMenu(int count, ...);
void printGieson(const char* string);
void printRed(const char* string);
void printHealth(const int state);