/* Questo file contiene le dichiarazioni delle funzioni crea_mappa(), gioca(), termina_gioco().
 Continene anche le definizioni dei tipi utilizzati in gamelib.c:
 struct Giocatore, struct Zona, enum Stato_giocatore, enum Tipo_zona, enum Tipo_oggetto. */

#define USABLE_ITEMS_N 6

/*deve rappresentare i valori cucina, soggiorno, rimessa, strada, lungo_lago, uscita_campeggio.*/
typedef enum //Tipo_zona
{
    kitchen, living_room, shed, street, lakefront, camping_exit,
} Type_area;

/*deve rappresentare i valori cianfrusaglia, bende, coltello, pistola, benzina, chiavi_auto.*/
typedef enum //Tipo_oggetto
{
    rubbish, bandages, knife, handgun, gasoline, adrenalin
} Type_item;

 //deve rappresentare i valori morto, ferito, vivo.
typedef enum //Stato_giocatore
{
    dead, injured, alive
} State_player;

/*contiene i campi i) enum Tipo_zona zona, ii) enum Tipo_oggetto oggetto, iii) struct Zona ∗ zona_successiva.*/
typedef struct Area_s //Zona
{ 
    Type_area area;
    Type_item item;
    struct Area_s* next_area;
    int is_searched;
} Area;

/*contiene 
i) un campo enum Stato_giocatore stato,
ii) un campo che memorizza la posizione sulla mappa (struct Zona* posizione), e 
iii) un array zaino di 6 posizioni, ciascuna di tipo enum Tipo_oggetto: 
per ogni posizione si mantiene memorizzato il numero di oggetti di quel tipo trovati durante il gioco.*/
typedef struct Player_s //Giocatore
{
    State_player state;
    Area* position;
    Type_item backpack[USABLE_ITEMS_N];
    int steps; //number of areas passed
    int escaped; //boolean
} Player;

int create_map();
int start();
int quit();